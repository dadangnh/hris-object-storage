# HRIS Object Storage

[![pipeline status](https://gitlab.com/dadangnh/hris-object-storage/badges/master/pipeline.svg)](https://gitlab.com/dadangnh/hris-object-storage/-/commits/master)
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/5074/badge)](https://bestpractices.coreinfrastructure.org/projects/5074)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/44690bc332194c26a94dc8bb062a67f1)](https://www.codacy.com/gl/dadangnh/hris-object-storage/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=dadangnh/hris-object-storage&amp;utm_campaign=Badge_Grade)
[![coverage report](https://gitlab.com/dadangnh/hris-object-storage/badges/master/coverage.svg)](https://gitlab.com/dadangnh/hris-object-storage/-/commits/master)

![Lines of code](https://img.shields.io/tokei/lines/gitlab.com/dadangnh/hris-object-storage)
![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/dadang/hris-object-storage_backend)
![Packagist License](https://img.shields.io/packagist/l/dadangnh/hris-object-storage)

![Packagist PHP Version Support](https://img.shields.io/packagist/php-v/dadangnh/hris-object-storage)
![Packagist Version](https://img.shields.io/packagist/v/dadangnh/hris-object-storage)
![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/dadangnh/hris-object-storage)
![Docker Image Version (latest semver)](https://img.shields.io/docker/v/dadang/hris-object-storage_backend)

Source code service HRIS Object Storage based on [Api-Platform](https://api-platform.com/) and [Symfony Framework](https://symfony.com/).

## Canonical source

The canonical source of hris-object-storage where all development takes place is [hosted on GitLab.com](https://gitlab.com/dadangnh/hris-object-storage).

## Installation

Please check [INSTALLATION.md](INSTALLATION.md) for the installation instruction.

## Contributing

This is an open source project, and we are very happy to accept community contributions.

# License

This code is published under [GPLv3 License](LICENSE).
