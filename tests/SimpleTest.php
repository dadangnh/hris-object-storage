<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SimpleTest extends ApiTestCase
{
    public function testHomePage(): void
    {
        // Expect to get 401 status code because no Token supplied
        try {
            $response = static::createClient()->request('GET', '/');
        } catch (TransportExceptionInterface $e) {
        }
        self::assertResponseStatusCodeSame(200);
    }
}
